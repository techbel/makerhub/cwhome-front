# Project Initialization
## Project creation
- `git clone https://github.com/primefaces/sakai-ng.git`
- Save Sakai & Git READMEs and create new blank README.md


## Build project
- `npm i`
//primeicons throws errors, so I execute with the flag --legacy-peer-deps:
//`npm i --legacy-peer-deps`
//At build-time, we can see vulnerability issues. Execute next command to solve them:
//    doc: https://docs.npmjs.com/cli/v8/commands/npm-audit
//`npm audit fix --force`


## Adapt Layout
- Create home module, with a simple home component (‘home works’) within it
    `ng g m home --routing`
    `ng g c home`
- Move Sakai demo to docs/demo-sakai
- Adapt app-routing.module.ts
- app/layout/app.menu.component.ts:
    - Adapt all routerLinks
- Top menu links
    - Delete unnecessary buttons
- Hide sidebar by default 
    - comment on app.layout.component.html
    - set LayoutService.state.staticMenuDesktopInactive as 'true'
- Set Theme sidepanel gear button to the bottom
    - in assets/layout/styles/layout/_config.scss, replace 'top: 50%;' par 'bottom: 0%';
- Add Toasts
    - Add markup on app.layout.component.html
    - Add ToastModule in app.layout.module.ts
    - Add MessageService as a provider in app.module

## App.module
- Delete unused imports (from demo)
- Reogranize imports (Angular, PrimeNG, Applications)
- Delete unused modules(from demo)
- Reorganize modules imports
- Providers:
    - Delete unused services (from demo)
- Manage french locale (for dates)
    - Add in providers: {provide: LOCALE_ID, useValue: 'fr'},
    - Add at bottom of 'Angular' imports: 
        import { LOCALE_ID} from '@angular/core';
        import localeFr from '@angular/common/locales/fr'
        import {registerLocaleData} from '@angular/common';
        registerLocaleData(localeFr);
- add modules on 'imports': CommonModule, HttpClientModule, FormsModule, ReactiveFormsModule
- add newly created HomeModule on 'imports'
- Reorganize modules (Angular, PrimeNG, Application)


## Change theme by default
- index.html
    `<link id="theme-css" rel="stylesheet" type="text/css" href="assets/layout/styles/theme/saga-green/theme.css">`
- app.layout.service.ts: change 'theme' to 'saga-green',


## Project structure
- create folder 'src/assets/app/images/.gitkeep
- create 'src/app/share/' folder containing folders:
    - compos/.gitkeep
    - guards/.gitkeep
    - models/.gitkeep
    - pipes/.gitkeep
    - services/.gitkeep
    - styles/.gitkeep
    - types/.gitkeep
    - utils/.gitkeep
    - validators/.gitkeep

- Create 404 page in ‘src/app/share/compos/notfound’
    - Adapt routing


## Environment Variables
o Use env var to store API connections


## Documentation
o Add compodoc cmd: `ng add @compodoc/compodoc`

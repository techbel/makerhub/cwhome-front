// Angular
import { NgModule } from '@angular/core';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID} from '@angular/core';
import localeFr from '@angular/common/locales/fr'
import {registerLocaleData} from '@angular/common';
registerLocaleData(localeFr);

// PrimeNG - Sakai

// Application
import { AppLayoutModule } from './layout/app.layout.module';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './home/home.module';
import { AppComponent } from './app.component';
import { NotfoundComponent } from './shared/compos/notfound/notfound.component';
import { MessageService } from 'primeng/api';
import { TestComponent } from './shared/compos/test/test.component';
import { TestService } from './shared/compos/test/test.service';


@NgModule({
    declarations: [
        AppComponent, 
        NotfoundComponent, TestComponent
    ],
    imports: [
        // Angular
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        // PrimeNG
        AppLayoutModule,
        // Application
        AppRoutingModule,
        HomeModule,
    ],
    providers: [
        // Angular
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        { provide: LOCALE_ID, useValue: 'fr' },
        // PrimeNG - Sakai
        MessageService,
        // Application
        TestService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

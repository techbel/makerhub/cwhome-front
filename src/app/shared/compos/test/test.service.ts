import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class TestService {
    private _apiUrl: string = environment.apiUrl;

    constructor(private _httpC: HttpClient) { }

    getHelloWorld(): Observable<string> {
        return this._httpC.get(this._apiUrl, {responseType: 'text'})
    }
}

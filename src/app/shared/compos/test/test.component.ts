import { Component, OnInit } from '@angular/core';
import { TestService } from './test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  helloWorldMsg: string = "";


  constructor(
    private readonly _testService: TestService
  ) { }

  ngOnInit(): void {
    this._testService.getHelloWorld().subscribe(msg => this.helloWorldMsg = msg);
  }
}
